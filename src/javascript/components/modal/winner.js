import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const winner = createFighterImage(fighter);
  showModal({title: 'Winner', bodyElement: winner});
}
